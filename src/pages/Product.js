// import coursesData from "../data/coursesData";
import {useEffect, useState} from "react";

import ProductCard from "../components/ProductCard";
import { Card, Button,Image,Table,Container,Col,Row,ButtonGroup,Form,InputGroup} from "react-bootstrap";

export default function Product(){

    //State tha will be used to store the courses retrieved form the database
     const [products, setProduct] = useState([]);

    //Retrieve the courses from the database upon initial render of the Course component.
    useEffect(() =>{


        fetch(`${process.env.REACT_APP_API_URL}/products/viewall-active-product`)
        .then(res => res.json())
        .then(data => {
            setProduct(data.map(product =>{

                return(
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
    },[])
    return(
        <>
            <h1 className="text-center mt-3">Products</h1>
              <Container>
      <Row className="justify-content-lg-center">
        
          {products}
     

      
      </Row>
      
    </Container> 
         
        </>
    )
}

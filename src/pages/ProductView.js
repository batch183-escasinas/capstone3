import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";
import Image from "react-bootstrap/Image";
import {FaCartPlus} from "react-icons/fa";

export default function ProductView(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [productImage, setProductImage] = useState();
	const imagePath=`${process.env.REACT_APP_API_URL}/${productImage}`;
	let listProduct=[];

	function AddToCart(){
		const list=JSON.parse(localStorage.getItem('productList'));
			
		if(list!=null){
			//console.log("not null");
			listProduct=list;
			

			console.log(listProduct.indexOf(productId));
			if(listProduct.indexOf(productId)==-1){
				listProduct.push(productId);
				localStorage.setItem(productId, 1);
			}else{
				Swal.fire({
                               title: "Product Aldeady Added",
                            icon: "error",
                             text: "You can change quantity in Cart"
                  })
			}

			localStorage.setItem('productList', JSON.stringify(listProduct));
			

		

		}else{
			
			listProduct.push(productId);
			localStorage.setItem('productList', JSON.stringify(listProduct));	
			localStorage.setItem(productId, 1);

		}

			
	} 
	useEffect(() =>{	
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price.toLocaleString());
			setProductImage(data.productImage);
		})
		
	}, [productId]);

	return(
		<>

		 <Container className="mt-5">
      <Row>
        <Col lg={{ span: 3, offset: 1 }}><Image src={`${imagePath}`} fluid thumbnail /></Col>
        <Col lg={{ span: 8}}>
        <Card>
      <Card.Header><h1>{name}</h1></Card.Header>
      <Card.Body>
        <Card.Title>Description:</Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
         <Card.Title>Price:</Card.Title>
   			<Card.Text>₱ {price}</Card.Text>
         <Button  variant="success" className="justifyContent" onClick={AddToCart}><FaCartPlus/> Add To Cart →</Button>
      </Card.Body>
    </Card>
       
					</Col>
      </Row>
     
    </Container>
		

 

		</>
	)
}
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';

import {Row,Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
function Login() {
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function loginUser(loginData){
		loginData.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Ahl Lightning!"
				});
			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}

		})
			setEmail("");
			setPassword("");
	}
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() =>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])


  return (
  		(user.id !== null)
		?
			(user.isAdmin)
			?
				<Navigate to="/admin"/>
			:
				<Navigate to="/product"/>
		:
		<>


<Row className="justify-content-center mt-5">
			 <Col lg="4">
			 
		 <Card className="text-center" border="primary">
      <Card.Header>	<h1 className="text-center">Login</h1></Card.Header>
      <Card.Body>
        
			<Form onSubmit ={(e) => loginUser(e)}>
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Email address</Form.Label>
				  <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
				  <Form.Text className="text-muted">
				    We'll never share your email with anyone else.
				  </Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password">
				  <Form.Label>Password</Form.Label>
				  <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
				</Form.Group>
				{
					isActive
					?
						<Button variant="primary" type="submit" id="submitBtn">
						  Submit
						</Button>
					:
						<Button variant="primary" type="submit" id="submitBtn" disabled>
						  Login
						</Button>
				}
			</Form>


      </Card.Body>
      <Card.Footer className="text-muted">Don't have an account yet? <a href="/register">Register here</a></Card.Footer>
    </Card>
		</Col>
			 </Row>	
		</>
  );
}

export default Login;
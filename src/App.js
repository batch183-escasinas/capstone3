

import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {Container} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from "./components/AppNavbar";
import CheckOut from "./components/CheckOut";
import Login from "./pages/Login";
import { UserProvider } from "./UserContext";
import Register from "./pages/Register";
import Home from "./pages/Home";
import Product from "./pages/Product";
import AdminDashboard from "./pages/AdminDashboard";
import ProductView from "./pages/ProductView";
import Logout from "./pages/Logout";


import 'mdb-react-ui-kit/dist/css/mdb.min.css'
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  const unsetUser = () =>{
    console.log("test");
    localStorage.removeItem("token");
  }


  useEffect(() =>{
   
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])
  return (
    <div>

    <UserProvider value={{user, setUser, unsetUser}}>
   <Router>
          <AppNavbar />

          <Container fluid>
              
              <Routes>
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/home" element={<Home />} />
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/product" element={<Product />} />

                  <Route exact path ="/admin" element={<AdminDashboard />} />
                  <Route exact path ="/product/:productId" element={<ProductView />} />
                  <Route exact path ="/logout" element={<Logout />} />
                   <Route exact path ="/checkout/:orderId" element={<CheckOut />} />
             
              </Routes>
          </Container>
        </Router>
         </UserProvider>
     </div>
  );
}

export default App;
